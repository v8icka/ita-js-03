import React  from 'react'

export const ContactForm = (props) => {
    const contact = props.contact
    
    return (
      <div className="row">
        <div className="col-md-6">
          <div className="form-group">
            <label htmlFor="nameInput" className="col-sm-2">Name</label>
            <div className="col-sm-10">
              <input type="text" className="form-control" name="name" id="nameInput" value={contact.name} onChange={props.handleInputChange.bind(this)} />
            </div>
          </div>

          <div className="form-group">
            <label htmlFor="phoneInput" className="col-sm-2">Phone</label>
            <div className="col-sm-10">
              <input type="text" className="form-control" name="phone" id="phoneInput" value={contact.phone} placeholder="Phone"  onChange={props.handleInputChange.bind(this)} />
            </div>
          </div>

          <div className="form-group">
            <label htmlFor="addressInput" className="col-sm-2">Address</label>
            <div className="col-sm-10">
              <textarea className="form-control"  name="address" id="addressInput" value={contact.address} placeholder="Adress" onChange={props.handleInputChange.bind(this)} ></textarea>
            </div>
          </div>

        </div>

        <div className="col-md-6">

          <div className="form-group">
            <label htmlFor="noteInput" className="col-sm-2">Note</label>
            <div className="col-sm-10">
              <textarea className="form-control" id="noteInput" name="note" value={contact.note} placeholder="Note" onChange={props.handleInputChange.bind(this)} ></textarea>
            </div>
          </div>
        </div>
      </div>
    )
}