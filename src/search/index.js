import React from 'react'
import { connect } from 'react-redux'

function mapStateToProps(state) {
  return {search: state.searchReducer.searchValue}
}

const actions = {
  onSearch: (e) => {
    return {type: 'SEARCH', payload: e.target.value}
  }
}

const Search = ({onSearch, searchValue}) => {
  return (
    <form className="navbar-form navbar-left" role="search">
      <div className="form-group">
        <input type="text" className="form-control" placeholder="Search" onChange={onSearch} value={searchValue} />
      </div>
    </form>
  )
}

export default connect(mapStateToProps, actions)(Search)