const initialState = {
  searchValue: ''
}

export const reducer = (state = initialState, action) => {
  switch(action.type) {
    case 'SEARCH':
      return Object.assign({}, state, {searchValue: action.payload})

    default:
      return state
  }
}