import React from 'react'
import ReactDOM from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'

import App from './app'
import { IndexReducer } from './index-reducer'
import IndexSagas from './index-sagas'
import './app.css'

const sagaMiddleware = createSagaMiddleware()

const store = createStore(
  IndexReducer,
  applyMiddleware(sagaMiddleware)
)

sagaMiddleware.run(IndexSagas)

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
)
