const initialState = {
  contracts: [],
  contract: {},
  requesting: false,
  requestingDelete: false,
  successfulCreate: false,
  successfulFetch: false,
  successfulEdit: false,
  successfulDelete: false,
  errors: []
}

export const reducer = (state = initialState, action) => {
  switch(action.type) {
    // LIST
    case 'CONTRACTS_REQUESTING':
      return Object.assign({}, state, {requesting: true, successfulFetch: false, successfulDelete: false, successfulCreate: false})

    case 'CONTRACTS_REQUEST_SUCCESS':
      return Object.assign({}, state, {requesting: false, successfulFetch: true, contracts: action.payload})

    // SHOW
    case 'CONTRACT_REQUESTING':
      return Object.assign({}, state, {requesting: true, successfulFetch: false})

    case 'CONTRACT_REQUEST_SUCCESS':
      return Object.assign({}, state, {successfulEdit: false, successfulFetch: true, requesting: false, contract: action.payload})

    // NEW
    case 'CONTRACT_CREATE_REQUESTING':
      return Object.assign({}, state, {requesting: true})

    case 'CONTRACT_CREATE_REQUEST_SUCCESS':
      return Object.assign({}, state, {successfulCreate: true, requesting: false})

    // EDIT
    case 'CONTRACT_EDIT_REQUESTING':
      return Object.assign({}, state, {successfulEdit: false, successfulFetch: false, requesting: true})

    case 'CONTRACT_EDIT_REQUEST_SUCCESS':
      return Object.assign({}, state, {successfulEdit: true, requesting: false})

    // DELETE
    case 'CONTRACT_DELETE_REQUESTING':
      return Object.assign({}, state, { requestingDelete: true})

    case 'CONTRACT_DELETE_REQUEST_SUCCESS':
      return Object.assign({}, state, {successfulDelete: true, requestingDelete: false})

    // ERROR
    case 'CONTRACTS_REQUEST_ERROR':
      return Object.assign({}, state, {successfulDelete: true, errors: [{message: 'Baaaaaaad, realy baaaad'}]})

    default:
      return state
  }
}



