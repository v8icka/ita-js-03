import React, { Component } from 'react'
import { connect } from 'react-redux'
import { reduxForm, Field } from 'redux-form'

import { actions } from './actions'
import { actions as contactsActions } from '../contacts/actions'

const mapStateToProps = (state) => {
  return { 
    successfulCreate: state.contractsReducer.successfulCreate,
    contacts: state.contactsReducer.contacts
  }
}

const mapDispatchToProps = {
  contactsRequesting: contactsActions.contactsRequesting,
  contractCreateRequesting: actions.contractCreateRequesting
}

class ContractEdit extends Component {
  constructor(props) {
    super(props)

    props.contactsRequesting()
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.successfulCreate) {
      this.props.history.push('/contracts')
    }
  }

  submitForm = (data) => {
    this.props.contractCreateRequesting(data)
  }

  render() {
    const {
      handleSubmit,
      contacts
    } = this.props
    
    return (
     
      <div className="container">

        <h2 className="">New Contract</h2>

          <form className="form-horizontal mt-30" onSubmit={handleSubmit(this.submitForm)}>
            <div className="row">
              <div className="col-md-6">
                <div className="form-group">
                  <label htmlFor="titleInput" className="col-sm-2">Title</label>
                  <div className="col-sm-10">
                    <Field component="input" type="text" className="form-control" name="title" id="titleInput" />
                  </div>
                </div>

                <div className="form-group">
                  <label htmlFor="priceInput" className="col-sm-2">Price</label>
                  <div className="col-sm-10">
                    <Field component="input" type="text" className="form-control" name="price" id="priceInput" />
                  </div>
                </div>

                <div className="form-group">
                  <label htmlFor="contact_id" className="col-sm-2">Contact</label>
                  <div className="col-sm-10">
                    <Field component="select" name="contact_id" className="form-control">
                      {contacts.map(c => <option key={c.id} value={c.id}>{c.name}</option>)}
                    </Field>
                  </div>
                </div>
              </div>

              <div className="col-md-6">

                <div className="form-group">
                  <label htmlFor="noteInput" className="col-sm-2">Note</label>
                  <div className="col-sm-10">
                    <Field component="textarea" className="form-control" id="noteInput" name="note" />
                  </div>
                </div>
              </div>
            </div>

            <div className="mt-30">
              <button
                className="btn btn-success"
                action="submit">Save contract</button>
            </div>
          </form>
      </div>
    )
  }
}

const formComponent = reduxForm({
  form: 'contractCreateForm'
})(ContractEdit)

export default connect(mapStateToProps, mapDispatchToProps)(formComponent)