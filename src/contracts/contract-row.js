import React from 'react'
import { Link } from 'react-router-dom'

export const ContractRow = (props) => {
  return (
    <tr>
      <td><Link to={`/contracts/${props.id}`}>{props.title}</Link></td>
      <td>{props.price}</td>
      <td>{props.note}</td>
      <td>...</td>
    </tr>
  )
}
