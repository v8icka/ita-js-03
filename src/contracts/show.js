import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

import { actions } from './actions'

const mapStateToProps = (state) => {
  const { contract, successfulFetch, successfulDelete, requesting } = state.contractsReducer
  return { contract, successfulFetch, successfulDelete, requesting }
}

class Contact extends Component {
  constructor(props) {
    super(props)

    props.contractRequesting(this.props.match.params.id)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.successfulDelete) {
      this.props.history.push('/contracts')
    }
  }

  render() {
    const {
      contract,
      requesting,
      successfulFetch,
      contractDeleteRequesting
    } = this.props

    return (
      <div className="container">
        {requesting && <span>requesting...</span>}

        {successfulFetch &&
          <div>
            <h1>{contract.title}</h1>

            <div className="row mt-30">
              <div className="col-md-6">
                <div className="row mb-10">
                  <strong className="col-sm-2">
                    Title
                  </strong>
                  <div className="col-sm-10">
                    {contract.title}
                  </div>
                </div>

                <div className="row mb-10">
                  <strong className="col-sm-2">
                    Price
                  </strong>
                  <div className="col-sm-10">
                    {contract.price}
                  </div>
                </div>
              </div>

              <div className="col-md-6">
                <div className="row mb-10">
                  <strong className="col-sm-2">
                    Note
                  </strong>
                  <div className="col-sm-10">
                    {contract.note}
                  </div>
                </div>
              </div>
            </div>

            <div className="mt-30">
              <Link to={`/contracts/${contract.id}/edit`} className="btn btn-default">Edit contract</Link>
              <button
                className="btn btn-danger"
                onClick={() => contractDeleteRequesting(contract.id)}>Delete contract</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

export default connect(mapStateToProps, actions)(Contact)