import { call, put, takeLatest } from 'redux-saga/effects'

import { actions } from './actions'

const { 
  contractsRequestSuccess,
  contractRequestSuccess,
  contractCreateRequestSuccess,
  contractEditRequestSuccess,
  contractDeleteRequestSuccess,
  contractsRequestError } = actions

const baseUrl = 'http://localhost:1234'
const contractsUrl = 'contracts'

function handleRequest(request) {
  return request
    .then(response => response.json())
    .then(json => json)
    .catch((error) => { throw error })
}

// LIST
function contractsRequestUrl() {
  const url = `${baseUrl}/${contractsUrl}`
  const request = fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
  })
  return handleRequest(request)
}

function* contractsRequest(action) {
  try {
    const contracts = yield call(contractsRequestUrl)
    yield put(contractsRequestSuccess(contracts))
  } catch (error) {
    yield put(contractsRequestError(error))
  }
}

// SHOW
function contractRequestUrl(id) {
  const url = `${baseUrl}/${contractsUrl}/${id}`
  const request = fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
  })
  return handleRequest(request)
}

function* contractRequest(action) {
  try {
    const contract = yield call(contractRequestUrl, action.id)
    yield put(contractRequestSuccess(contract))
  } catch (error) {
    yield put(contractsRequestError(error))
  }
}

// NEW
function contractCreateRequestUrl(data) {
  const url = `${baseUrl}/${contractsUrl}`
  const request = fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
  return handleRequest(request)
}

function* contractCreateRequest(action) {
  try {
    const contract = yield call(contractCreateRequestUrl, action.data)
    yield put(contractCreateRequestSuccess(contract))
  } catch (error) {
    yield put(contractsRequestError(error))
  }
}

// EDIT
function contractEditRequestUrl(data) {
  const url = `${baseUrl}/${contractsUrl}/${data.id}`
  const request = fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
  return handleRequest(request)
}

function* contractEditRequest(action) {
  try {
    const contract = yield call(contractEditRequestUrl, action.data)
    yield put(contractEditRequestSuccess(contract))
  } catch (error) {
    yield put(contractsRequestError(error))
  }
}

// DELETE
function contractDeleteRequestUrl(contract) {
  const url = `${baseUrl}/${contractsUrl}/${contract}`
  const request = fetch(url, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  return handleRequest(request)
}

function* contractDeleteRequest(action) {
  try {
    const contract = yield call(contractDeleteRequestUrl, action.contract)
    yield put(contractDeleteRequestSuccess(contract))
  } catch (error) {
    yield put(contractsRequestError(error))
  }
}

// WATCHER
function* contractsWatcher () {
  yield [
    takeLatest("CONTRACTS_REQUESTING", contractsRequest),
    takeLatest("CONTRACT_REQUESTING", contractRequest),
    takeLatest("CONTRACT_CREATE_REQUESTING", contractCreateRequest),
    takeLatest("CONTRACT_EDIT_REQUESTING", contractEditRequest),
    takeLatest("CONTRACT_DELETE_REQUESTING", contractDeleteRequest)]
}

export default contractsWatcher