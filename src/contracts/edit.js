import React, { Component } from 'react'
import { connect } from 'react-redux'
import { reduxForm, Field } from 'redux-form'

import { actions } from './actions'

const mapStateToProps = (state) => {
  const { contract, successful, requesting, successfulEdit } = state.contractsReducer
  return { contract, successful, successfulEdit, requesting, initialValues: contract }
}

class ContractEdit extends Component {
  constructor(props) {
    super(props)

    props.contractRequesting(this.props.match.params.id)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.successfulEdit) {
      this.props.history.push('/contracts')
    }
  }

  submitForm = (data) => {
    this.props.contractEditRequesting(data)
  }

  render() {
    const {
      contract,
      handleSubmit
    } = this.props

    console.log(this.props.match)
    return (
      <div className="container">

        <h2 className="">Edit contract</h2>

        {contract &&
          <form className="form-horizontal mt-30" onSubmit={handleSubmit(this.submitForm)}>
            <div className="row">
              <div className="col-md-6">
                <div className="form-group">
                  <label htmlFor="titleInput" className="col-sm-2">Name</label>
                  <div className="col-sm-10">
                    <Field component="input" type="text" className="form-control" name="title" id="titleInput" />
                  </div>
                </div>

                <div className="form-group">
                  <label htmlFor="priceInput" className="col-sm-2">Phone</label>
                  <div className="col-sm-10">
                    <Field component="input" type="text" className="form-control" name="price" id="priceInput" />
                  </div>
                </div>

              </div>

              <div className="col-md-6">

                <div className="form-group">
                  <label htmlFor="noteInput" className="col-sm-2">Note</label>
                  <div className="col-sm-10">
                    <Field component="textarea" className="form-control" id="noteInput" name="note" />
                  </div>
                </div>
              </div>
            </div>

            <div className="mt-30">
              <button
                className="btn btn-success"
                action="submit">Save contract</button>
            </div>
          </form>
        }
      </div>
    )
  }
}

const formComponent = reduxForm({
  form: 'contractEditForm',
  enableReinitialize: true
})(ContractEdit)

export default connect(mapStateToProps, actions)(formComponent)