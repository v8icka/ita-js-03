import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import { ContractRow } from './contract-row'
import { actions } from './actions'

function mapStateToProps(state) {
  const {contracts, requesting, successfulFetch} = state.contractsReducer
  return {contracts, requesting, successfulFetch, search: state.searchReducer.searchValue}
  
}

class Contracts extends Component {
  constructor(props) {
    super(props)

    props.contractsRequesting()
  }

  getFilteredContracts(contracts, search) {
    return contracts.filter((contract) => {
      return JSON.stringify(Object.values(contract)).toLowerCase().includes(search.toLowerCase())
    })
  }

  render() {
    const {
      requesting,
      successfulFetch,
      contracts,
      search
    } = this.props

    return (
    <div className="container">
      <h2>Contracts</h2>

      <div className="row mt-30">
        <div className="col-sm-6">
          <Link to="/contracts/new" className="btn btn-success">New contract</Link>
        </div>
      </div>
      
      {requesting && <span className="align-center">Loading contracts...</span>}

      {!requesting && successfulFetch && contracts.length === 0 && 
        <span className="">Oops, there is no contract yet :)</span>}
      
      {!requesting && successfulFetch && contracts.length > 0 &&
        <table className="table table-hover mt-30">
          <thead>
            <tr>
              <th>Title</th>
              <th>Price</th>
              <th>Note</th>
              <th>Client</th>
            </tr>
          </thead>

          <tbody>
            {this.getFilteredContracts(contracts, search).map((contract) => {
              return <ContractRow key={contract.id} {...contract} />
            })}

          </tbody>
        </table>}

        {/* ERROR MESSAGES WILL COME HERE */}
      </div>
    )
  }
}

export default connect(mapStateToProps, actions)(Contracts)