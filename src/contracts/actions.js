export const actions = {

  // LIST
  contractsRequesting: () => {
    return {type: 'CONTRACTS_REQUESTING'}
  },

  contractsRequestSuccess: (contracts) => {
    return {type: 'CONTRACTS_REQUEST_SUCCESS', payload: contracts}
  },

  contractsRequestError: (error) => {
    return {type: 'CONTRACTS_REQUEST_ERROR', payload: error}
  },

  // SHOW
  contractRequesting: (id) => {
    return {type: 'CONTRACT_REQUESTING', id}
  },

   // NEW
  contractCreateRequesting: (data) => {
    return {type: 'CONTRACT_CREATE_REQUESTING', data}
  },

  contractCreateRequestSuccess: () => {
    return {type: 'CONTRACT_CREATE_REQUEST_SUCCESS'}
  },

  contractRequestSuccess: (contract) => {
    return {type: 'CONTRACT_REQUEST_SUCCESS', payload: contract}
  },

   // EDIT
  contractEditRequesting: (data) => {
    return {type: 'CONTRACT_EDIT_REQUESTING', data}
  },

  contractEditRequestSuccess: (contract) => {
    return {type: 'CONTRACT_EDIT_REQUEST_SUCCESS', payload: contract}
  },

   // DELETE
  contractDeleteRequesting: (contract) => {
    return {type: 'CONTRACT_DELETE_REQUESTING', contract}
  },

  contractDeleteRequestSuccess: () => {
    return {type: 'CONTRACT_DELETE_REQUEST_SUCCESS', message: 'Coool, you deleted it. You rock!'}
  }
}