import { combineReducers } from 'redux'
import { reducer as form } from 'redux-form'

import { reducer as authReducer } from './auth/reducer'
import { reducer as searchReducer } from './search/reducer'
import { reducer as contactsReducer } from './contacts/reducer'
import { reducer as contractsReducer } from './contracts/reducer'

export const IndexReducer = combineReducers({
  authReducer,
  searchReducer,
  contactsReducer,
  contractsReducer,
  form
})
