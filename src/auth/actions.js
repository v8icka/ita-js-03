export const actions = {
  loginRequest: () => {
    return { type: 'LOGIN_REQUEST' }
  },
  logoutRequest: () => {
    return {type: 'LOGOUT_REQUEST'}
  }
}