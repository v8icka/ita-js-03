import React from 'react'
import { connect } from 'react-redux'
import { actions } from './actions'

function mapStateToProps(state) {
  return { loggedIn: state.loggedIn }
}

const Login = ({ loginRequest }) => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-sm-6 col-sm-offset-3">
          <h2 className="">Sign in</h2>

          <div className="form-group">

            <div className="col-sm-9 pv-10">
              <input type="text" className="form-control" placeholder="password" />
            </div>

            <div className="col-sm-3 pv-10">
              <button onClick={loginRequest} className="btn btn-success">Go</button>
            </div>

          </div>
        </div>
      </div>
    </div>
  )
}

export default connect(mapStateToProps, actions)(Login)