const initialState = {
  loggedIn: false
}

export const reducer = function (state = initialState, action) {
  switch (action.type) {
    case 'LOGIN_REQUEST':
      return Object.assign({}, state, { loggedIn: true })

    case 'LOGOUT_REQUEST':
      return Object.assign({}, state, { loggedIn: false })
          
    default:
      return state
  }
}