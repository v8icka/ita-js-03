import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import { ContactRow } from './contact-row'
import { actions } from './actions'

function mapStateToProps(state) {
  const {contacts, requesting, successfulFetch} = state.contactsReducer
  return {contacts, requesting, successfulFetch, search: state.searchReducer.searchValue}
  
}

class Contacts extends Component {
  constructor(props) {
    super(props)

    props.contactsRequesting()
  }

  getFilteredContacts(contacts, search) {
    return contacts.filter((contact) => {
      return JSON.stringify(Object.values(contact)).toLowerCase().includes(search.toLowerCase())
    })
  }

  render() {
    const {
      requesting,
      successfulFetch,
      contacts,
      search
    } = this.props

    return (
    <div className="container">
      <h2>Contacts</h2>

      <div className="row mt-30">
        <div className="col-sm-6">
          <Link to="contacts/new" className="btn btn-success">New contact</Link>
        </div>
      </div>
      
      {requesting && <span className="align-center">Loading contacts...</span>}

      {!requesting && successfulFetch && contacts.length === 0 && 
        <span className="">Oops, there is no contact yet :)</span>}
      
      {!requesting && successfulFetch && contacts.length > 0 &&
        <table className="table table-hover mt-30">
          <thead>
            <tr>
              <th>Name</th>
              <th>Phone</th>
              <th>Adress</th>
              <th>Note</th>
            </tr>
          </thead>

          <tbody>
            {this.getFilteredContacts(contacts, search).map((contact) => {
              return <ContactRow key={contact.id} {...contact} />
            })}

          </tbody>
        </table>}

        {/* ERROR MESSAGES WILL COME HERE */}
      </div>
    )
  }
}

export default connect(mapStateToProps, actions)(Contacts)