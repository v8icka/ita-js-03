export const actions = {

  // LIST
  contactsRequesting: () => {
    return {type: 'CONTACTS_REQUESTING'}
  },

  contactsRequestSuccess: (contacts) => {
    return {type: 'CONTACTS_REQUEST_SUCCESS', payload: contacts}
  },

  contactsRequestError: (error) => {
    return {type: 'CONTACTS_REQUEST_ERROR', payload: error}
  },

  // SHOW
  contactRequesting: (id) => {
    return {type: 'CONTACT_REQUESTING', id}
  },

   // NEW
  contactCreateRequesting: (data) => {
    return {type: 'CONTACT_CREATE_REQUESTING', data}
  },

  contactCreateRequestSuccess: () => {
    return {type: 'CONTACT_CREATE_REQUEST_SUCCESS'}
  },

  contactRequestSuccess: (contact) => {
    return {type: 'CONTACT_REQUEST_SUCCESS', payload: contact}
  },

   // EDIT
  contactEditRequesting: (data) => {
    return {type: 'CONTACT_EDIT_REQUESTING', data}
  },

  contactEditRequestSuccess: (contact) => {
    return {type: 'CONTACT_EDIT_REQUEST_SUCCESS', payload: contact}
  },

   // DELETE
  contactDeleteRequesting: (contact) => {
    return {type: 'CONTACT_DELETE_REQUESTING', contact}
  },

  contactDeleteRequestSuccess: () => {
    return {type: 'CONTACT_DELETE_REQUEST_SUCCESS', message: 'Coool, you deleted it. You rock!'}
  }
}