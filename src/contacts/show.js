import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

import { actions } from './actions'

const mapStateToProps = (state) => {
  const { contact, successfulFetch, successfulDelete, requesting } = state.contactsReducer
  return { contact, successfulFetch, successfulDelete, requesting }
}

class Contact extends Component {
  constructor(props) {
    super(props)

    props.contactRequesting(this.props.match.params.id)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.successfulDelete) {
      this.props.history.push('/')
    }
  }

  render() {
    const {
      contact,
      requesting,
      successfulFetch,
      contactDeleteRequesting
    } = this.props

    return (
      <div className="container">
        {requesting && <span>requesting...</span>}

        {successfulFetch &&
          <div>
            <h1>{contact.name}</h1>

            <div className="row mt-30">
              <div className="col-md-6">
                <div className="row mb-10">
                  <strong className="col-sm-2">
                    Name
                  </strong>
                  <div className="col-sm-10">
                    {contact.name}
                  </div>
                </div>

                <div className="row mb-10">
                  <strong className="col-sm-2">
                    Phone
                  </strong>
                  <div className="col-sm-10">
                    {contact.phone}
                  </div>
                </div>

                <div className="row mb-10">
                  <strong className="col-sm-2">
                    Adress
                  </strong>
                  <div className="col-sm-10">
                    {contact.address}
                  </div>
                </div>
              </div>

              <div className="col-md-6">
                <div className="row mb-10">
                  <strong className="col-sm-2">
                    Note
                  </strong>
                  <div className="col-sm-10">
                    {contact.note}
                  </div>
                </div>
              </div>
            </div>

            <div className="mt-30">
              <Link to={`/contacts/${contact.id}/edit`} className="btn btn-default">Edit</Link>
              <button
                className="btn btn-danger"
                onClick={() => contactDeleteRequesting(contact.id)}>Delete contact</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

export default connect(mapStateToProps, actions)(Contact)