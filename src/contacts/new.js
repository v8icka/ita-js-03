import React, { Component } from 'react'
import { connect } from 'react-redux'
import { reduxForm, Field } from 'redux-form'

import { actions } from './actions'

const mapStateToProps = (state) => {
  const { successfulCreate } = state.contactsReducer
  return { successfulCreate }
}

class ContactEdit extends Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.successfulCreate) {
      this.props.history.push('/')
    }
  }

  submitForm = (data) => {
    this.props.contactCreateRequesting(data)
  }

  render() {
    const {
      handleSubmit
    } = this.props

    return (
      <div className="container">

        <h2 className="">New Contact</h2>

          <form className="form-horizontal mt-30" onSubmit={handleSubmit(this.submitForm)}>
            <div className="row">
              <div className="col-md-6">
                <div className="form-group">
                  <label htmlFor="nameInput" className="col-sm-2">Name</label>
                  <div className="col-sm-10">
                    <Field component="input" type="text" className="form-control" name="name" id="nameInput" />
                  </div>
                </div>

                <div className="form-group">
                  <label htmlFor="phoneInput" className="col-sm-2">Phone</label>
                  <div className="col-sm-10">
                    <Field component="input" type="text" className="form-control" name="phone" id="phoneInput" />
                  </div>
                </div>

                <div className="form-group">
                  <label htmlFor="addressInput" className="col-sm-2">Address</label>
                  <div className="col-sm-10">
                    <Field component="textarea" className="form-control" name="address" id="addressInput" />
                  </div>
                </div>

              </div>

              <div className="col-md-6">

                <div className="form-group">
                  <label htmlFor="noteInput" className="col-sm-2">Note</label>
                  <div className="col-sm-10">
                    <Field component="textarea" className="form-control" id="noteInput" name="note" />
                  </div>
                </div>
              </div>
            </div>

            <div className="mt-30">
              <button
                className="btn btn-success"
                action="submit">Save contact</button>
            </div>
          </form>
      </div>
    )
  }
}

const formComponent = reduxForm({
  form: 'contactCreateForm'
})(ContactEdit)

export default connect(mapStateToProps, actions)(formComponent)