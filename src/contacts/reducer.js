const initialState = {
  contacts: [],
  contact: {},
  requesting: false,
  requestingDelete: false,
  successfulCreate: false,
  successfulFetch: false,
  successfulEdit: false,
  successfulDelete: false,
  errors: []
}

export const reducer = (state = initialState, action) => {
  switch(action.type) {
    // LIST
    case 'CONTACTS_REQUESTING':
      return Object.assign({}, state, {requesting: true, successfulFetch: false, successfulDelete: false, successfulCreate: false})

    case 'CONTACTS_REQUEST_SUCCESS':
      return Object.assign({}, state, {requesting: false, successfulFetch: true, contacts: action.payload})

    // SHOW
    case 'CONTACT_REQUESTING':
      return Object.assign({}, state, {requesting: true, successfulFetch: false})

    case 'CONTACT_REQUEST_SUCCESS':
      return Object.assign({}, state, {successfulEdit: false, successfulFetch: true, requesting: false, contact: action.payload})

    // NEW
    case 'CONTACT_CREATE_REQUESTING':
      return Object.assign({}, state, {requesting: true})

    case 'CONTACT_CREATE_REQUEST_SUCCESS':
      return Object.assign({}, state, {successfulCreate: true, requesting: false})

    // EDIT
    case 'CONTACT_EDIT_REQUESTING':
      return Object.assign({}, state, {successfulEdit: false, successfulFetch: false, requesting: true})

    case 'CONTACT_EDIT_REQUEST_SUCCESS':
      return Object.assign({}, state, {successfulEdit: true, requesting: false})

    // DELETE
    case 'CONTACT_DELETE_REQUESTING':
      return Object.assign({}, state, { requestingDelete: true})

    case 'CONTACT_DELETE_REQUEST_SUCCESS':
      return Object.assign({}, state, {successfulDelete: true, requestingDelete: false})

    // ERROR
    case 'CONTACTS_REQUEST_ERROR':
      return Object.assign({}, state, {successfulDelete: true, errors: [{message: 'Baaaaaaad, realy baaaad'}]})

    default:
      return state
  }
}



