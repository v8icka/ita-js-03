import React, { Component } from 'react'
import { connect } from 'react-redux'
import { reduxForm, Field } from 'redux-form'

import { actions } from './actions'

const mapStateToProps = (state) => {
  const { contact, successful, requesting, successfulEdit } = state.contactsReducer
  return { contact, successful, successfulEdit, requesting, initialValues: contact }
}

class ContactEdit extends Component {
  constructor(props) {
    super(props)

    props.contactRequesting(this.props.match.params.id)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.successfulEdit) {
      this.props.history.push('/')
    }
  }

  submitForm = (data) => {
    this.props.contactEditRequesting(data)
  }

  render() {
    const {
      contact,
      handleSubmit
    } = this.props

    return (
      <div className="container">

        <h2 className="">Edit contact</h2>

        {contact &&
          <form className="form-horizontal mt-30" onSubmit={handleSubmit(this.submitForm)}>
            <div className="row">
              <div className="col-md-6">
                <div className="form-group">
                  <label htmlFor="nameInput" className="col-sm-2">Name</label>
                  <div className="col-sm-10">
                    <Field component="input" type="text" className="form-control" name="name" id="nameInput" />
                  </div>
                </div>

                <div className="form-group">
                  <label htmlFor="phoneInput" className="col-sm-2">Phone</label>
                  <div className="col-sm-10">
                    <Field component="input" type="text" className="form-control" name="phone" id="phoneInput" />
                  </div>
                </div>

                <div className="form-group">
                  <label htmlFor="addressInput" className="col-sm-2">Address</label>
                  <div className="col-sm-10">
                    <Field component="textarea" className="form-control" name="address" id="addressInput" />
                  </div>
                </div>

              </div>

              <div className="col-md-6">

                <div className="form-group">
                  <label htmlFor="noteInput" className="col-sm-2">Note</label>
                  <div className="col-sm-10">
                    <Field component="textarea" className="form-control" id="noteInput" name="note" />
                  </div>
                </div>
              </div>
            </div>

            <div className="mt-30">
              <button
                className="btn btn-success"
                action="submit">Save contact</button>
            </div>
          </form>
        }
      </div>
    )
  }
}

const formComponent = reduxForm({
  form: 'contactEditForm',
  enableReinitialize: true
})(ContactEdit)

export default connect(mapStateToProps, actions)(formComponent)