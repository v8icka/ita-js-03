import { call, put, takeLatest } from 'redux-saga/effects'

import { actions } from './actions'

const { 
  contactsRequestSuccess,
  contactRequestSuccess,
  contactCreateRequestSuccess,
  contactEditRequestSuccess,
  contactDeleteRequestSuccess,
  contactsRequestError } = actions

const baseUrl = 'http://localhost:1234'
const contactsUrl = 'contacts'

function handleRequest(request) {
  return request
    .then(response => response.json())
    .then(json => json)
    .catch((error) => { throw error })
}

// LIST
function contactsRequestUrl() {
  const url = `${baseUrl}/${contactsUrl}`
  const request = fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
  })
  return handleRequest(request)
}

function* contactsRequest(action) {
  try {
    const contacts = yield call(contactsRequestUrl)
    yield put(contactsRequestSuccess(contacts))
  } catch (error) {
    yield put(contactsRequestError(error))
  }
}

// SHOW
function contactRequestUrl(id) {
  const url = `${baseUrl}/${contactsUrl}/${id}`
  const request = fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
  })
  return handleRequest(request)
}

function* contactRequest(action) {
  try {
    const contact = yield call(contactRequestUrl, action.id)
    yield put(contactRequestSuccess(contact))
  } catch (error) {
    yield put(contactsRequestError(error))
  }
}

// NEW
function contactCreateRequestUrl(data) {
  const url = `${baseUrl}/${contactsUrl}`
  const request = fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
  return handleRequest(request)
}

function* contactCreateRequest(action) {
  try {
    const contact = yield call(contactCreateRequestUrl, action.data)
    yield put(contactCreateRequestSuccess(contact))
  } catch (error) {
    yield put(contactsRequestError(error))
  }
}

// EDIT
function contactEditRequestUrl(data) {
  const url = `${baseUrl}/${contactsUrl}/${data.id}`
  const request = fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
  return handleRequest(request)
}

function* contactEditRequest(action) {
  try {
    const contact = yield call(contactEditRequestUrl, action.data)
    yield put(contactEditRequestSuccess(contact))
  } catch (error) {
    yield put(contactsRequestError(error))
  }
}

// DELETE
function contactDeleteRequestUrl(contact) {
  const url = `${baseUrl}/${contactsUrl}/${contact}`
  const request = fetch(url, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  return handleRequest(request)
}

function* contactDeleteRequest(action) {
  try {
    const contact = yield call(contactDeleteRequestUrl, action.contact)
    yield put(contactDeleteRequestSuccess(contact))
  } catch (error) {
    yield put(contactsRequestError(error))
  }
}

// WATCHER
function* contactsWatcher () {
  yield [
    takeLatest("CONTACTS_REQUESTING", contactsRequest),
    takeLatest("CONTACT_REQUESTING", contactRequest),
    takeLatest("CONTACT_CREATE_REQUESTING", contactCreateRequest),
    takeLatest("CONTACT_EDIT_REQUESTING", contactEditRequest),
    takeLatest("CONTACT_DELETE_REQUESTING", contactDeleteRequest)]
}

export default contactsWatcher