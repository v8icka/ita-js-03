import React from 'react'
import { Link } from 'react-router-dom'

export const ContactRow = (props) => {
  return (
    <tr>
      <td><Link to={`/contacts/${props.id}`}>{props.name}</Link></td>
      <td>{props.phone}</td>
      <td>{props.address}</td>
      <td>...</td>
    </tr>
  )
}
