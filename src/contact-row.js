import ContactsSaga from './contacts/sagas'

export default function* IndexSaga () {
  yield [
    ContactsSaga
  ]
}
