import React from 'react'
import { Link, NavLink } from 'react-router-dom'
import { connect } from 'react-redux'

import { actions } from './auth/actions'

import Search from './search'

function mapStateToProps(state) {
  return {loggedIn: state.authReducer.loggedIn}
}

const Navbar = ({loggedIn, logoutRequest}) => {
  return (
    <div className="navbar clearfix header">
      <div className="container">
        <Link to="/" className="navbar-brand">App</Link>
        
        <NavLink to="/" className="navbar-brand">Contacts</NavLink>
        <NavLink to="/contracts" className="navbar-brand">Contracts</NavLink>

        {(loggedIn && 
          <div>
            <button className="btn btn-default navbar-btn pull-right" onClick={logoutRequest}>Logout</button>
            
            <Search />
          </div>) || <Link to="/" className="btn btn-default navbar-btn pull-right">Login</Link>
        }
      </div>
    </div>
  )
}

export default connect(mapStateToProps, actions)(Navbar)