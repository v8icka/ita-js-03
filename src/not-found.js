import React from 'react'

export const NotFoundRoute = (props) => {
  return (
    <div className="container alert alert-danger">
      <h2>Page not found (404)</h2>

      <p><strong>{props.location.pathname}</strong> couldn't be found</p>
      
      <p>Sorry, but the page you are looking for has not been found.</p>

      <ul>
        <li>Is the URL written correctly?</li>
        <li>If yes than the content you are looking for doesn't exist, has moved or ...</li>
        <li>We may have some problems on the server :/</li>
      </ul>

      <p>Hit the refresh button on your browser to try it again.</p>
    </div>
  )
} 
