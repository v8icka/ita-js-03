import ContactsSaga from './contacts/sagas'
import ContractsSaga from './contracts/sagas'

export default function* IndexSagas () {
  yield [
    ContactsSaga(),
    ContractsSaga()
  ]
}