export const ROUTES = {
  CONTACTS: '/',
  CONTACTS_NEW: '/contacts/new',
  CONTACTS_SHOW: '/contacts/:id',
  CONTACTS_EDIT: '/contacts/:id/edit',

  CONTRACTS: '/contracts',
  CONTRACTS_NEW: '/contracts/new',
  CONTRACTS_SHOW: '/contracts/:id',
  CONTRACTS_EDIT: '/contracts/:id/edit'
}