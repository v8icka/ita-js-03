import React from 'react'
import { Route, Switch, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

import './app.css'
import Navbar from './navbar'
import { ROUTES } from './routes'

import Contacts from './contacts'
import Contact from './contacts/show'
import ContactEdit from './contacts/edit'
import ContactNew from './contacts/new'

import Contracts from './contracts'
import Contract from './contracts/show'
import ContractEdit from './contracts/edit'
import ContractNew from './contracts/new'
import Login from './auth'
import { NotFoundRoute } from './not-found'

function mapStateToProps(state) {
  return {
    loggedIn : state.authReducer.loggedIn
  }
}

const App = (props) => {
  return (
    <div className="App">
      <Navbar/>
      {
        (props.loggedIn &&
          <Switch>
            <Route path={ROUTES.CONTRACTS_EDIT} component={ContractEdit} />
            <Route path={ROUTES.CONTRACTS_NEW} component={ContractNew} />
            <Route path={ROUTES.CONTRACTS_SHOW} component={Contract} />
            <Route path={ROUTES.CONTRACTS} component={Contracts} />

            <Route path={ROUTES.CONTACTS_EDIT} component={ContactEdit} />
            <Route path={ROUTES.CONTACTS_NEW} component={ContactNew} />
            <Route path={ROUTES.CONTACTS_SHOW} component={Contact} />
            <Route path={ROUTES.CONTACTS} component={Contacts} />
            <Route component={NotFoundRoute} />
          </Switch>) || <Login />
      }
    </div>
  )
}

export default withRouter(connect(mapStateToProps)(App))