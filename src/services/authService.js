let loggedIn = false

export class AuthService {
  static login() {
    loggedIn = true
  }

  static logout() {
    loggedIn = false
  }

  static isLoggedIn() {
    return loggedIn
  }
}