import _ from 'lodash'
import $http from 'axios'

export class ContactsService {
  static async getContacts() {
    return _.values((await $http.get('http://localhost:1234/contacts')).data)
  }

  static async getContact(id) {
    return (await $http.get(`http://localhost:1234/contacts/${id}`)).data
  }

  static async update(contact) {
    return (await $http.post(`http://localhost:1234/contacts/${contact.id}`, contact)).data
  }

  static async create(data) {
    return (await $http.post(`http://localhost:1234/contacts`, data))
  }

  static async delete(contact) {
    return (await $http.delete(`http://localhost:1234/contacts/${contact.id}`))
  }
}
