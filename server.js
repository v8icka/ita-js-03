const bodyParser = require('body-parser')
const express = require('express')
const knex = require('knex')
const Model = require('objection').Model

const api = express()

const db = knex({
  client: 'pg',
  connection: {
    user: 'postgres',
    database: 'ita',
    password: 'trk'
  }
})

Model.knex(db)

class Base extends Model {
  static get tableName() {
    return this.name.toLocaleLowerCase()
  }

  static async findById(id) {
    return await this.query().where({id: id}).limit(1).first();
  }
}

class Contact extends Base {
}

class Contract extends Base {
}

api.use(bodyParser.json())

api.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE')
  next()
})

api.get('/contacts', async (req, res) => {
  const c = await Contact.query()

  c ? res.status(200).send(c) : res.status(404).end()
})

api.post('/contacts', async (req, res) => {
  c = await Contact.query().insert(req.body)
  console.log(c)
  res.status(200).send(c)
})

api.get('/contacts/:id', async (req, res) => {
   const c = await Contact.findById(req.params.id)

   c ? res.status(200).send(c) : res.status(404).end()
})

api.post('/contacts/:id', async (req, res) => {
  const c = await Contact.findById(req.params.id)
  const u = await c.$query().patchAndFetchById(req.params.id, req.body)

  res.status(200).send(u)
})

api.delete('/contacts/:id', async (req, res) => {
  const c = await Contact.findById(req.params.id)

  await c.$query().del()
  res.status(204).end()
})

api.get('/contracts', async (req, res) => {
  const c = await Contract.query()

  c ? res.status(200).send(c) : res.status(404).end()
})

api.post('/contracts', async (req, res) => {
  c = await Contract.query().insert(req.body)
  console.log(c)
  res.status(200).send(c)
})

api.get('/contracts/:id', async (req, res) => {
   const c = await Contract.findById(req.params.id)

   c ? res.status(200).send(c) : res.status(404).end()
})

api.post('/contracts/:id', async (req, res) => {
  const c = await Contract.findById(req.params.id)
  const u = await c.$query().patchAndFetchById(req.params.id, req.body)

  res.status(200).send(u)
})

api.delete('/contracts/:id', async (req, res) => {
  const c = await Contract.findById(req.params.id)

  await c.$query().del()
  res.status(204).end()
})



api.listen(1234)
